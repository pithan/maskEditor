#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 16:16:34 2016

@author: Linus Pithan
Provies a simple mask editor for ID03 beamline
"""

import numpy
import fabio
from PyQt5.QtWidgets import QFileDialog
from silx.gui.plot import Plot2D
from silx.gui.plot.PlotActions import PlotAction 
from silx.gui import qt


class LoadImageAction(PlotAction):
    """Loads an image into the current plot

    :param plot: :class:`.PlotWidget` instance on which to operate
    :param parent: See :class:`QAction`
    """
    def __init__(self, plot, parent=None):
        PlotAction.__init__(self,
                            plot,
                            icon='document-open',
                            text='pixels intensity',
                            tooltip='Compute image intensity distribution',
                            triggered=self.loadImage,
                            checkable=False,
                            parent=parent)
     

            
    def loadImage(self):
        """Configer the open button"""
        fname=QFileDialog.getOpenFileName(self.plot, 'Open file', '/home')

        if fname[0]:
            print(fname[0])
            obj = fabio.open(fname[0])
            self.plot.addImage(obj.data)

                
def main():

    app=qt.QApplication([])
    
    plotImage=Plot2D()
    myaction=LoadImageAction(plotImage,parent=app)
    toolBar=plotImage.toolBar()
    toolBar.addAction(myaction)
    
    plotImage.show()
    app.exec_()

main()